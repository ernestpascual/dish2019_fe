import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import ViewDiploma from "./viewdiploma"
import Learning from "./learning"
import "../daya.css"



// Each logical "route" has two components, one for
// the sidebar and one for the main area. We want to
// render both of them in different places when the
// path matches the current URL.
const routes = [
  {
    path: "/",
    exact: true,
    sidebar: () => <div>home!</div>,
    main: () => <h2>Welcome!</h2>
  },
  {
    path: "/learning",
    sidebar: () => <div>bubblegum!</div>,
    main: () => <Learning />
  },
  {
    path: "/diploma",
    sidebar: () => <div>shoelaces!</div>,
    main: ()  => <ViewDiploma />
  }
];



class SidebarExample extends React.Component {
  constructor (props) {
    super(props)
    this.state = {account_balance: ""}
  }


async componentDidMount() { 
  setInterval(async() => {
  await fetch('http://127.0.0.1:30001/getAccount/ernpas/false', {
        mode: 'cors' // 'cors' by default
      }).then(response => response.json())
      .then(json => {
        console.log(json)
        this.setState({
          account_balance : json["balance"]
        })
      })
    }, 2000)
}
  
  render() {


  return (
    <Router>
      <div style={{ display: "flex" }}>
        <div
          className="container"
        >
          <ul style={{ listStyleType: "none", padding: 0 }}>
            <li className="link" >
              <Link to="/">Home</Link>
            </li>
            <li className="link">
              <Link to="/learning">Learning System</Link>
            </li>
            <li className="link">
              <Link to="/diploma"> View Diploma</Link>
            </li>
          </ul>

          {routes.map((route, index) => (
            // You can render a <Route> in as many places
            // as you want in your app. It will render along
            // with any other <Route>s that also match the URL.
            // So, a sidebar or breadcrumbs or anything else
            // that requires you to render multiple things
            // in multiple places at the same URL is nothing
            // more than multiple <Route>s.
            <Route
              key={index}
              path={route.path}
              exact={route.exact}
            />
           
          ))}
           <p style = {{"margin-left" : "1vw"}}> Balance: {this.state.account_balance} </p>

        </div>

        <div style={{ flex: 1, padding: "10px"  ,width: "100%"}}>
          {routes.map((route, index) => (
            // Render more <Route>s with the same paths as
            // above, but different components this time.
            <Route
              key={index}
              path={route.path}
              exact={route.exact}
              component={route.main}
            />
          ))}
        </div>
      </div>
    </Router>
  );
}
}

export default SidebarExample;

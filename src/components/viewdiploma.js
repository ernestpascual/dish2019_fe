import React from 'react';
import InputBase from '@material-ui/core/InputBase';
import Button from '@material-ui/core/Button';
import '../diploma.css'


class SidebarExample extends React.Component {
    constructor (props) {
      super(props)
      this.state = {diploma : "diplo", name: "", degree: "", authorized_by: "", value: ""}
      this.handleClick = this.handleClick.bind(this);
      this.handleChange = this.handleChange.bind(this);
    }
    
    handleClick() {
      fetch('http://127.0.0.1:3020/getDiploma?txnHash=' + this.state.value, {
        mode: 'cors' // 'cors' by default
      })
    .then(response => response.json())
    .then(json => {
        console.log(json)
    /*
    this.setState({
      name : json["transaction"]["actions"][0]["data"].substring(1,  ).split(",")[0].slice(1,-1),
      degree : json["transaction"]["actions"][0]["data"].substring(1,  ).split(",")[1].slice(1,-1),
      authorized_by : json["transaction"]["actions"][0]["data"].substring(1,  ).split(",")[2].slice(1,-2)
        })
        */

        this.setState({
            name : json["name"],
            degree : json["degree"],
            authorized_by : json["authorized_by"]
              })
    })
    }

    handleChange(e) {
        this.setState({value: e.target.value});
      }
    

    render() {
       
    return ( 
    
    <div className = "container-search" >
        <h1> Search Diploma: </h1>
        <InputBase placeholder="Enter Hash of diploma" value={this.state.value}
        onChange={this.handleChange}/>
        <Button variant="contained" color="primary" onClick={this.handleClick} >
        Search
        </Button>
        <p> { this.state.name } </p>
        <p> { this.state.degree } </p>
        <p> { this.state.authorized_by } </p>
     

    </div>
  );
}
    }



export default SidebarExample;
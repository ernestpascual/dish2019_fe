import React from 'react';
import InputBase from '@material-ui/core/InputBase';
import Button from '@material-ui/core/Button';
import '../diploma.css'


function SearchAppBar(props) {
  return (
    <div className = "container-search" >
        <h1> Course: Biology 2020 </h1>
        <h2> Four-legged whales once straddled land and sea </h2>
        <p className="learning-p" > Whales belong in the ocean, right? That may be true today, but cetaceans (whales, dolphins, porpoises) actually descended from four legged mammals that once lived on land. New research published in Current Biology reports the discovery in Peru of an entirely new species of ancestral whale that straddled land and sea, providing insight into the weird evolutionary journey of our mammalian friends. </p>
        <p className="learning-p" > We might think of them as smooth, two-flippered ocean swimmers that struggle to even survive the Thames, but whales originated more than 50 million years ago from artiodactyls—land-dwelling, hoofed mammals. </p>
        <p className="learning-p" > Initially, whales’ ancestors resembled small deer, with four toes, each one ending in a small hoof. One particular fossilized “missing link” found in India suggests that the last whale precursors took to the water in times of danger but came onto land to give birth and eat. They would spend considerable time wading in shallow water, foraging for aquatic vegetation and invertebrates, and eventually small fish and amphibians.</p> 

        <h2> Quiz </h2>
        <h4> Whales belong in the ocean, right? </h4>
        <Button variant="contained" color="primary"  style = {{ margin: "10px" }} >
        yes
      </Button>
      <Button variant="contained" color="primary" >
        no
      </Button>

    </div>
  );
}



export default SearchAppBar;